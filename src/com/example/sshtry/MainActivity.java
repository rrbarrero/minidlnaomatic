package com.example.sshtry;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import com.example.sshtry.MyConnect;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends Activity {
	
	String TAG = "AVISO!";
	ImageView statusSign;
	Context contexto;
	Integer connMode; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		statusSign = (ImageView) findViewById(R.id.statusSign);
		
		this.contexto = getApplicationContext();
		
		NetworkUtils nu = new NetworkUtils(contexto);
		this.connMode = nu.getConnectionMode();
		
		Timer myTimer = new Timer();
		CheckServerStatus myTimerTask = new CheckServerStatus();
		
		Log.d(TAG, "iniciando checkeo");
		myTimer.scheduleAtFixedRate(myTimerTask, 0, 5000);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	 
	        case R.id.action_settings:
	            Intent intent = new Intent(this, UserSetttingsActivity.class);
	            startActivity(intent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	public void callStop(View view){
		MyConnect myc = new MyConnect();
		myc.setView(view);
		myc.setFunctionToExec("stop");
		myc.execute();
	}
	
	public void callStart(View view){
		MyConnect myc = new MyConnect();
		myc.setView(view);
		myc.setFunctionToExec("start");
		myc.execute();
	}
	
	public void callShutdown(final View view){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						MyConnect myc = new MyConnect();
						myc.setView(view);
						myc.setFunctionToExec("shutdown");
						myc.execute();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						break;
				}
				
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Sure?");
		builder.setPositiveButton("Yes", dialogClickListener);
		builder.setNegativeButton("Cancel", dialogClickListener);
		builder.show();
		
	}
	
	private Handler puente = new Handler(){
		@Override
		public void handleMessage(Message msg){
			if((Boolean)msg.obj){
				statusSign.setImageResource(R.drawable.online);
			} else {
				statusSign.setImageResource(R.drawable.offline);
			}
		}
	};
	
	private class CheckServerStatus extends TimerTask{
			
		@Override
		public void run() {
			
			String ipAddress = null;
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(contexto);
			
			if (connMode == NetworkUtils.GSM_MODE){
				ipAddress = sharedPref.getString("ssh_ext_hostname", "");
			} else if (connMode == NetworkUtils.WIFI_MODE){
				ipAddress = sharedPref.getString("ssh_lan_hostname", "");
			} 
			if ((connMode == NetworkUtils.UNKNOWN_MODE) || (ipAddress == "")){
				Message msg = new Message();
				msg.obj = false;
				return;
			}
			Log.d(TAG, "ip: " + ipAddress);
			final String address = ipAddress;
			
			Thread th = new Thread(new Runnable(){
				@Override
				public void run(){
					Log.d(TAG, "Lanzado el nuevo hilo");		
					try {
						Boolean ServerStatus = InetAddress.getByName(address).isReachable(5000);
						Log.d(TAG, "El estado del servidor es " + ServerStatus.toString());
						Message msg = new Message();
						if(ServerStatus){
							msg.obj = true;
						} else {
							msg.obj = false;
						}
						puente.sendMessage(msg);
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			th.start();
		}
		
	}

}
