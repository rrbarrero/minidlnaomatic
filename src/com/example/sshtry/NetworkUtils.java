package com.example.sshtry;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;

public class NetworkUtils {
	
	Context contexto;
	public static final Integer UNKNOWN_MODE = 0;
	public static final Integer WIFI_MODE = 1;
	public static final Integer GSM_MODE = 2;
	private State mobile;
	
	public NetworkUtils(Context contexto){
		this.contexto = contexto;
	}
	
	public Integer getConnectionMode(){
		
		ConnectivityManager conMan = (ConnectivityManager) contexto.getSystemService(
				Context.CONNECTIVITY_SERVICE);
		try{
			State mobile = conMan.getNetworkInfo(0).getState();
		}catch(NullPointerException e){
			State mobile = android.net.NetworkInfo.State.UNKNOWN;
		}
		State wifi = conMan.getNetworkInfo(1).getState();
		
		if (mobile == android.net.NetworkInfo.State.CONNECTED){
			return NetworkUtils.GSM_MODE;
		} else if (wifi == android.net.NetworkInfo.State.CONNECTED){
			return NetworkUtils.WIFI_MODE;
		} else{
			return NetworkUtils.UNKNOWN_MODE;
		}
	}

}
